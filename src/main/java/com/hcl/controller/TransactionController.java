package com.hcl.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.dto.TrasnsctionDto;
import com.hcl.model.Trasnsction;
import com.hcl.service.TransacService;

@RestController
public  class TransactionController {
	
	@Autowired
	TransacService transactionservice;
	
	private TrasnsctionDto trasnsctionDto = new TrasnsctionDto();
	
	@PostMapping("/transaction")
	public ResponseEntity<Object> transaction(@RequestBody Trasnsction trasnsction) {	
		if(transactionservice.transaction(trasnsction).isStatus()) {
			trasnsctionDto=	transactionservice.transaction(trasnsction);
				trasnsctionDto.setMessage("Transaction is sucessfull");	
			return new ResponseEntity<>(trasnsctionDto,HttpStatus.OK);
		}
		
		else {
			trasnsctionDto=	transactionservice.transaction(trasnsction);
			trasnsctionDto.setMessage("Transaction is Failure");
			return new ResponseEntity<>(trasnsctionDto,HttpStatus.BAD_REQUEST);
		
	}


}

	
	
}
